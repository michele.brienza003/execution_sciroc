# Motions

Tutorial for launch the talker node and assign the word for execution of sign.  
~~~~
$ rosrun demo_motions talker
~~~~

Before launch the tiago in gazebo simulation

~~~~
$ roslaunch tiago_gazebo tiago_gazebo.launch
~~~~

For motion launch the node with command

~~~~
$ roslaunch demo_motions motions.launch 
~~~~

The launch file then loads the corresponding yaml file which contains the motions for the right model type. The motion is selected manually by node "talker".
