// C++ standard headers
#include <exception>
#include <typeinfo>

// Boost headers
#include <boost/shared_ptr.hpp>

// ROS headers
#include <ros/ros.h>
#include <ros/package.h>
#include <actionlib/client/simple_action_client.h>
#include <play_motion_msgs/PlayMotionAction.h>

// C++ standard headers
#include <cstdlib>
#include <yaml-cpp/yaml.h>

#include <regex>
#include<algorithm>
#include <iostream> 
#include <string> 
#include <vector> 


int main(int argc, char** argv)
{  ros::init(argc, argv, "run_motion");  
  std::vector<std::string> motions_vec, descriptions_vec;
  std::string yaml_name = ros::package::getPath("execution") + "/resources/lis_motions.yaml";
  YAML::Node node;
  node = YAML::LoadFile(yaml_name);
  YAML::Node node_play_motion = node["play_motion"];
  YAML::Node node_motions = node_play_motion["motions"];
  for(YAML::const_iterator it=node_motions.begin(); it!= node_motions.end(); it++)
  {
    motions_vec.push_back(it->first.as<std::string>());
    YAML::Node node_motion = node_motions[it->first.as<std::string>()];
    YAML::Node node_meta   = node_motion["meta"];
    descriptions_vec.push_back(node_meta["description"].as<std::string>());
  }
  ROS_INFO("Nodo di esecuzione avviato");

  ros::NodeHandle nh;
  if (!ros::Time::waitForValid(ros::WallDuration(10.0))) 
  {
    ROS_FATAL("Timed-out waiting for valid time.");
    return EXIT_FAILURE;
  }
  actionlib::SimpleActionClient<play_motion_msgs::PlayMotionAction> client("/play_motion", true);

  while(ros::ok())
  {  
  std::string sign = "";
  std::string words = "|";
  std::string frase = "phase execution2";

  std::string motions[6] = {};
  int pos = 0;
  int posv = 0;
  std::string token;
  nh.getParam("/sign",sign);  
  if(sign.find(frase) != std::string::npos){
  while ((pos = sign.find(words)) != std::string::npos) {
    token = sign.substr(0, pos);
    motions[posv] = token; 
    std::cout << motions[posv] << std::endl;
    sign.erase(0, pos + (words).length());
    posv++;
    
  }
  motions[posv] = sign;
  nh.setParam("/sign","");
}
  int success = strcmp(motions[0].c_str() ,frase.c_str());
    if(success == 0)
    {
        client.waitForServer();
        for(int i = 1; i < 6; i++) {
        play_motion_msgs::PlayMotionGoal goal;
	motions[i].erase(remove(motions[i].begin(), motions[i].end(), '}'),motions[i].end());
	motions[i].erase(remove(motions[i].begin(), motions[i].end(), ' '),motions[i].end());	 
	motions[i].erase(remove(motions[i].begin(), motions[i].end(), '{'),motions[i].end());
	motions[i].erase(remove(motions[i].begin(), motions[i].end(), '?'),motions[i].end());	
	motions[i].erase(remove(motions[i].begin(), motions[i].end(), ','),motions[i].end());	
        goal.motion_name = motions[i];
        

        goal.priority = 0;

        ROS_INFO_STREAM("Gesto da eseguire: " << goal.motion_name);
        client.sendGoal(goal);


        bool actionOk = client.waitForResult(ros::Duration(90.0));

        actionlib::SimpleClientGoalState state = client.getState();

        if ( actionOk )
            ROS_INFO_STREAM("Gesto eseguito con successo : " << state.toString());
        else
            ROS_ERROR_STREAM("Gesto fallito, errore : " << state.toString());
        }
      motions[0] = "";
     
    }
  }
}
