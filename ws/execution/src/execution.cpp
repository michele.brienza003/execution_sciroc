#include <string>

// Boost headers
#include <boost/shared_ptr.hpp>

// ROS headers
#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <ros/topic.h>
#include <regex>
#include <algorithm>
#include <iostream> 
#include <string> 
#include <vector>
#include <fstream>
#include <sstream>
#include <stdlib.h>

using namespace std;


typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> arm_control_client;
typedef boost::shared_ptr< arm_control_client>  arm_control_client_Ptr;
typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> hand_control_client;
typedef boost::shared_ptr< hand_control_client>  hand_control_client_Ptr;

void inizio();
void buongiorno();
void limone();
void desidera();
void grazie();
void caffe();
void birraallaspina();
void wc();
void the();
void dica();
void cocacola();
void cracker();
void frullatore();
void bicchiere();
void acqua();
void gommadamasticare();
void patate();
void cibo();
void bere();
void panino();
void euro();
void tovagliolo();
void macchiato();
void frizzante();
void rubinetto();
void vinorosso();
void dopo();
void forno();
void succodifrutta();
void ciao();
void saluti();
void lattecaldo();
void lattefreddo();
void uno();
void perfavore();
void nonce();
void midispiace();
void destra();
void miscusiconto();

void createArmClient(arm_control_client_Ptr& actionClient)
{
  ROS_INFO("Creating action client to arm controller ...");

  actionClient.reset( new arm_control_client("/arm_controller/follow_joint_trajectory") );

  int iterations = 0, max_iterations = 3;
  while( !actionClient->waitForServer(ros::Duration(2.0)) && ros::ok() && iterations < max_iterations )
  {
    ROS_DEBUG("Waiting for the arm_controller_action server to come up");
    ++iterations;
  }

  if ( iterations == max_iterations )
    throw std::runtime_error("Error in createArmClient: arm controller action server not available");
}

void createHandClient(arm_control_client_Ptr& actionClient)
{
  ROS_INFO("Creating action client to hand controller ...");

  actionClient.reset( new hand_control_client("/hand_controller/follow_joint_trajectory") );

  int iterations = 0, max_iterations = 3;
  while( !actionClient->waitForServer(ros::Duration(2.0)) && ros::ok() && iterations < max_iterations )
  {
    ROS_DEBUG("Waiting for the hand_controller_action server to come up");
    ++iterations;
  }

  if ( iterations == max_iterations )
    throw std::runtime_error("Error in createHandClient: hand controller action server not available");
}

void waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, double valori[])
{
  goal.trajectory.joint_names = {"arm_1_joint","arm_2_joint","arm_3_joint","arm_4_joint","arm_5_joint","arm_6_joint","arm_7_joint"};
  goal.trajectory.points.resize(1);

  int index = 0;
  goal.trajectory.points[index].positions.resize(7);
  for (int j = 0; j < 7; j++)
  {
    goal.trajectory.points[index].positions[j] = valori[j];
  }
  goal.trajectory.points[index].velocities.resize(7);
  goal.trajectory.points[index].velocities = {0.5,0.5,0.5,0.5,0.5,0.5,0.5};
  goal.trajectory.points[index].time_from_start = ros::Duration(0.75);

  }
void waypoints_arm_goal2(control_msgs::FollowJointTrajectoryGoal& goal, double valori[])
{
  goal.trajectory.joint_names = {"arm_1_joint","arm_2_joint","arm_3_joint","arm_4_joint","arm_5_joint","arm_6_joint","arm_7_joint"};
  goal.trajectory.points.resize(1);

  int index = 0;
  goal.trajectory.points[index].positions.resize(7);
  for (int j = 0; j < 7; j++)
  {
    goal.trajectory.points[index].positions[j] = valori[j];
  }
  goal.trajectory.points[index].velocities.resize(7);
  goal.trajectory.points[index].velocities = {0.5,0.5,0.5,0.5,0.5,0.5,0.5};
  goal.trajectory.points[index].time_from_start = ros::Duration(3.4);

  }

void waypoints_hand_goal(control_msgs::FollowJointTrajectoryGoal& goal, double valori[])
{
  goal.trajectory.joint_names = {"hand_index_joint", "hand_mrl_joint", "hand_thumb_joint"};

  goal.trajectory.points.resize(1);
  int index = 0;
  goal.trajectory.points[index].positions.resize(3);
  for (int j = 0; j < 3; j++)
  {
    goal.trajectory.points[index].positions[j] = valori[j];
  }

  goal.trajectory.points[index].velocities.resize(3);
  goal.trajectory.points[index].velocities = {0.6,0.6,0.6};
  goal.trajectory.points[index].time_from_start = ros::Duration(0.75);

  }



int main(int argc, char** argv)
{
  ros::init(argc, argv, "run_traj_control");


  ROS_INFO("Starting run_traj_control application ...");
  ros::NodeHandle nh;
  std::string sign;
  std::getline(std::cin,sign);
  

  if(sign == ("limone")  ){
      inizio();
      limone();

  }
  

 if(sign == ("buongiorno") ) {
     inizio();
     buongiorno();

 }

  if(sign == ("desidera") ) {
      inizio();
      desidera();

  }
    if(sign == ("wc") ) {
        inizio();
        wc();

    }
    if(sign == ("cracker") ) {
        inizio();
        cracker();

    }
    if(sign == ("cocacola") ) {
        inizio();
        cocacola();

    }
    if(sign == ("tovagliolo") ) {
        inizio();
        tovagliolo();

    }
    
    if(sign == ("caffe") ) {
        inizio();
        caffe();

    }
    
    if(sign == ("euro") ) {
        inizio();
        euro();

    }
    if(sign == ("grazie") ) {
        inizio();
        grazie();

    }
    if(sign == ("the") ) {
        inizio();
        the();

    }
    if(sign == ("acqua") ) {
        inizio();
        acqua();

    }
    
    if(sign == ("bicchiere") ) {
        inizio();
        bicchiere();

    }
    
    if(sign == ("frullatore") ) {
        inizio();
        frullatore();

    }
    
    if(sign == ("gommadamasticare") ) {
        inizio();
        gommadamasticare();

    }
    
    if(sign == ("birraallaspina") ) {
        inizio();
        birraallaspina();

    }
    
    if(sign == ("patate") ) {
        inizio();
        patate();

    }
    
    if(sign == ("bere") ) {
        inizio();
        bere();

    }
    
    if(sign == ("cibo") ) {
        inizio();
        cibo();

    }
    
    if(sign == ("panino") ) {
        inizio();
        panino();

    }
    if(sign == ("caffemacchiato") ) {
        inizio();
        caffe();
        macchiato();

    }
    if(sign == ("acquafrizzante") ) {
        inizio();
        acqua();
        frizzante();

    }
    if(sign == ("acquarubinetto") ) {
        inizio();
        acqua();
        rubinetto();

    }
    if(sign == ("vinorosso") ) {
        inizio();
        bere();
        vinorosso();

    }
    if(sign == ("patatealforno") ) {
        inizio();
        patate();
        forno();

    }
    if(sign == ("patatefritte") ) {
        inizio();
        patate();
        frizzante();

    }
    if(sign == ("succodifrutta") ) {
        inizio();
        bere();
        succodifrutta();

    }
    if(sign == ("buongiornodica") ) {
        inizio();
        buongiorno();
        dica();

    }
    if(sign == ("grazieciao") ) {
        inizio();
        grazie();
        ciao();

    }
    if(sign == ("graziesaluti") ) {
        inizio();
        grazie();
        saluti();

    }
    if(sign == ("lattecaldo") ) {
        inizio();
        lattecaldo();

    }
    if(sign == ("lattefreddo") ) {
        inizio();
        lattefreddo();

    }
    if(sign == ("caffedopo") ) {
        inizio();
        caffe();
        dopo();

    }
    if(sign == ("perfavoreuncaffe") ) {
        inizio();
        perfavore();
        caffe();
        uno();

    }
    if(sign == ("perfavoreunbicchiere") ) {
        inizio();
        perfavore();
        bicchiere();
        uno();

    }
    if(sign == ("wcuominiadestra") ) {
        inizio();
        wc();
        patate();
        destra();

    }
    if(sign == ("caffenoncemidispiace") ) {
        inizio();
        caffe();
        nonce();
        midispiace();

    }
    if(sign == ("uneuro") ) {
        inizio();
        uno();
        euro();

    }
    if(sign == ("conto") ) {
        inizio();
        miscusiconto();

    }
  return EXIT_SUCCESS;
}


void inizio() {
      hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;
  
    double vettorebase[7] = {0.20,-1.34,-0.2,1.94,-1.57,1.37,0.0};
    double vettorebasemano[3] = {0,0,0};

    waypoints_arm_goal(arm_goal,vettorebase);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(2);
    ArmClient->sendGoal(arm_goal);
    waypoints_hand_goal(hand_goal,vettorebasemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);

    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.3).sleep();
    }
 }

void rubinetto() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorirubinettobraccio[7] = {0.07,-0.9,-1.63,2.10,-0.9,-0.14,-2.07};
    waypoints_arm_goal(arm_goal,valorirubinettobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valorirubinettomano[3] = {3.1,3.3,0};
    waypoints_hand_goal(hand_goal,valorirubinettomano);
    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }

    double valorirubinettobraccio1[7] = {0.07,-0.7,-1.63,2.15,-1.7,-0.25,-1.58};
    waypoints_arm_goal(arm_goal,valorirubinettobraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorirubinettobraccio2[7] = {0.07,-1.07,-1.63,2.23,-1.16,0.31,-2.07};
    waypoints_arm_goal(arm_goal,valorirubinettobraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorirubinettobraccio3[7] = {0.07,-0.7,-1.63,2.15,-1.7,-0.25,-1.58};
    waypoints_arm_goal(arm_goal,valorirubinettobraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}
void uno() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoriunobraccio[7] = {0.62, -1.15, -2.47 , 2.28, -1.58 ,0.42,0.45};
    waypoints_arm_goal(arm_goal,valoriunobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valoriunomano[3] = {0,9,6};
    waypoints_hand_goal(hand_goal,valoriunomano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    double valoriunobraccio1[7] = {0.62, -1.27, -1.93 , 2.28, -1.58 ,1.02,-0.5};
    waypoints_arm_goal(arm_goal,valoriunobraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}

void destra() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoridestrabraccio[7] = {0.59, -0.79, -2.67, 2.28, -2.07, -1.39, -1.25};
    waypoints_arm_goal(arm_goal,valoridestrabraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valoridestramano[3] = {0,9,6};
    waypoints_hand_goal(hand_goal,valoridestramano);
    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    double valoridestrabraccio1[7] = {0.59, -0.79, -2.67, 2.00, -2.07, -1.39, -1.25};
    waypoints_arm_goal(arm_goal,valoridestrabraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}

void midispiace() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorimidispiacebraccio[7] = {0.28, -1.05, -1.97, 2.29, -0.70 , -0.17, -2.07};
    double valorimidispiacebraccio2[7] = {0.28, -1.05, -1.97, 2.29, -0.70 , -0.17, -1.87};
    double valorimidispiacebraccio3[7] = {0.28, -0.52, -2.03, 2.29, -0.70 , -0.17, -0.91};
    waypoints_arm_goal(arm_goal,valorimidispiacebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valorimidispiacemano[3] = {6,9,6};
    waypoints_hand_goal(hand_goal,valorimidispiacemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()) )
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorimidispiacebraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorimidispiacebraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}
void nonce() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorinoncebraccio[7] = {0.28, -0.80, -2.62, 2.29, -1.62 , -0.17, -1.87};
    double valorinoncebraccio2[7] = {0.28, -0.80, -2.62, 2.29, -0.70 , -0.17, -1.87};
    waypoints_arm_goal(arm_goal,valorinoncebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valorinoncemano[3] = {1.09,9,3.34};
    waypoints_hand_goal(hand_goal,valorinoncemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorinoncebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorinoncebraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorinoncebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}

void dica() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoridicabraccio[7] = {0.59, -1.00, -2.87, 2.05, -1.16 , 0.20, 0.71};
    waypoints_arm_goal(arm_goal,valoridicabraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valoridicamano[3] = {0,9,6};
    waypoints_hand_goal(hand_goal,valoridicamano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    double valoridicabraccio1[7] = {1.27, -1.00, -2.87, 2.29, -1.16 , 0.20, -0.87};
    waypoints_arm_goal(arm_goal,valoridicabraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoridicabraccio2[7] = {1.40, -1.00, -2.87, 1.87, -1.16 , 1.11, -0.87};
    waypoints_arm_goal(arm_goal,valoridicabraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}
void perfavore() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoriperfavorebraccio[7] = {0.62, -0.57, -3.12, 2.28, 2.06 , -0.14, 2.07};
    waypoints_arm_goal(arm_goal,valoriperfavorebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoriperfavoremano[3] = {0,0,0};
    waypoints_hand_goal(hand_goal,valoriperfavoremano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoriperfavorebraccio1[7] = {0.62, -0.82, -2.72, 2.28, 2.06 , -0.31, 1.7};
    waypoints_arm_goal(arm_goal,valoriperfavorebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}
void saluti () {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorisalutibraccio[7] = {0.38, -1.20, -1.7, 2.10, -1.74, 0.22, 0.25};
    waypoints_arm_goal(arm_goal,valorisalutibraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorisalutimano[3] = {0,0,0};
    double valorisalutimano1[3] = {6,9,0};
    waypoints_hand_goal(hand_goal,valorisalutimano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_hand_goal(hand_goal,valorisalutimano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_hand_goal(hand_goal,valorisalutimano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_hand_goal(hand_goal,valorisalutimano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}

void ciao () {
         hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoriciaobraccio[7] = {0.38, -1.15, -2.87, 2.27, -2.07, -0.25, -2.07};
    waypoints_arm_goal(arm_goal,valoriciaobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoriciaomano[3] = {0,0,0};
    waypoints_hand_goal(hand_goal,valoriciaomano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoriciaobraccio1[7] = {0.38, -1.15, -2.87, 2.27, -2.07, 0.22, -2.07};
    waypoints_arm_goal(arm_goal,valoriciaobraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriciaobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriciaobraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}


void vinorosso () {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorivinorossobraccio[7] = {0.44,-0.82,-2.32,2.21,-1.95,0.22,0.71};
    waypoints_arm_goal(arm_goal,valorivinorossobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorivinorossomano[3] = {0,9,6};
    waypoints_hand_goal(hand_goal,valorivinorossomano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorivinorossomano1[3] = {1.2,9,6};
    waypoints_hand_goal(hand_goal,valorivinorossomano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorivinorossomano2[3] = {4.3,9,6};
    waypoints_hand_goal(hand_goal,valorivinorossomano2);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_hand_goal(hand_goal,valorivinorossomano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_hand_goal(hand_goal,valorivinorossomano2);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}

void forno() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorifornobraccio[7] = {0.25,-0.82,-1.87,1.63,-0.33,-0.10,-0.33};
    waypoints_arm_goal(arm_goal,valorifornobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    
    double valorifornomano1[3] = {0,0,0};
    waypoints_hand_goal(hand_goal,valorifornomano1);
    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}


void frizzante() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorifrizzantebraccio[7] = {0.25,-0.82,-1.87,1.63,-0.45,-0.10,-0.33};
    waypoints_arm_goal(arm_goal,valorifrizzantebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorifrizzantemano[3] = {0,0,0};
    waypoints_hand_goal(hand_goal,valorifrizzantemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorifrizzantemano1[3] = {4.14,6.19,-2.62};
    waypoints_hand_goal(hand_goal,valorifrizzantemano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorifrizzantemano2[3] = {0,0,0};
    waypoints_hand_goal(hand_goal,valorifrizzantemano2);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorifrizzantemano3[3] = {4.14,6.19,-2.62};
    waypoints_hand_goal(hand_goal,valorifrizzantemano3);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}

void grazie() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorigraziebraccio[7] = {0.9,-0.92,-2.57,2.28,-1.74,0.22,0.25};
    waypoints_arm_goal(arm_goal,valorigraziebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorigraziemano[3] = {0.31,-0.39,2};
    waypoints_hand_goal(hand_goal,valorigraziemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valorigraziebraccio1[7] = {0.9,-0.92,-2.57,1.76,-1.74,0.22,0.25};
    waypoints_arm_goal(arm_goal,valorigraziebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorigraziemano1[3] = {0.31,-0.39,2};
    waypoints_hand_goal(hand_goal,valorigraziemano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}



void macchiato() {
    hand_control_client_Ptr HandClient;
createHandClient(HandClient);

control_msgs::FollowJointTrajectoryGoal hand_goal;
arm_control_client_Ptr ArmClient;
createArmClient(ArmClient);
control_msgs::FollowJointTrajectoryGoal arm_goal;


double valoricaffemacchiatobraccio[7] = {0.25,-0.82,-2,2.15,-1.16,0.25,-0.33};
waypoints_arm_goal(arm_goal,valoricaffemacchiatobraccio);
arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
ArmClient->sendGoal(arm_goal);
while(!(ArmClient->getState().isDone()) && ros::ok())
{
  ros::Duration(0.1).sleep();
}

double valoricaffemacchiatobraccio1[7] = {0.25,-0.82,-1.58,2.15,-1.16,0.25,-0.33};
waypoints_arm_goal(arm_goal,valoricaffemacchiatobraccio1);
arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
ArmClient->sendGoal(arm_goal);
while(!(ArmClient->getState().isDone()) && ros::ok())
{
  ros::Duration(0.1).sleep();
}

}

void caffe() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoricaffebraccio[7] = {0.07,-0.7,-1.23,2.08,-1.86,0.28,-0.37};
    waypoints_arm_goal(arm_goal,valoricaffebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    
    double valoricaffemano[3] = {2.27,9,-1.46};
    waypoints_hand_goal(hand_goal,valoricaffemano);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valoricaffebraccio1[7] = {0.07,-0.7,-1.23,2.08,-1.86,0.28,-1.45};
    waypoints_arm_goal(arm_goal,valoricaffebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.4);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valoricaffebraccio3[7] = {0.07,-0.7,-1.23,2.08,-1.86,0.28,-0.37};
    waypoints_arm_goal(arm_goal,valoricaffebraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.4);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    waypoints_arm_goal(arm_goal,valoricaffebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.4);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    waypoints_arm_goal(arm_goal,valoricaffebraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.4);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

}
void bicchiere() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoribicchierebraccio[7] = {0.49,-0.7,-1.04,2.08,-1.86,0.28,-0.37};
    waypoints_arm_goal(arm_goal,valoribicchierebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valoribicchieremano[3] = {3.26,4.77,-2.62};
    waypoints_hand_goal(hand_goal,valoribicchieremano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valoribicchierebraccio1[7] = {0.49,-0.7,-1.04,2.08,-1.86,0.28,-2};
    waypoints_arm_goal(arm_goal,valoribicchierebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valoribicchierebraccio3[7] = {0.49,-0.7,-1.04,2.08,-1.86,0.28,-0.37};
    waypoints_arm_goal(arm_goal,valoribicchierebraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

}

void acqua() {
    hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoriacquabraccio[7] = {0.07,-0.90,-1.63,2.28,0.34,-0.09,-2.07};
    double valoriacquabraccio1[7] = {0.07,-0.90,-1.63,2.28,-1.2,-0.09,-1.90};
    waypoints_arm_goal(arm_goal,valoriacquabraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valoriacquamano[3] = {0, 0, 0};
    waypoints_hand_goal(hand_goal,valoriacquamano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriacquabraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.4);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valoriacquabraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.4);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriacquabraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valoriacquabraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriacquabraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valoriacquabraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

}

void cracker() {
    hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoricrackerbraccio[7] = {0.25,-0.7,-1.82,1.95,-1.16,-0.25,-0.66};
    waypoints_arm_goal(arm_goal,valoricrackerbraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valoricrackermano[3] = {2.27,9,-1.46};
    waypoints_hand_goal(hand_goal,valoricrackermano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valoricrackerbraccio1[7] = {0.25,-0.70,-1.82,2.29,-1.16,-0.25,-0.66};
    waypoints_arm_goal(arm_goal,valoricrackerbraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoricrackerbraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoricrackerbraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    waypoints_arm_goal(arm_goal,valoricrackerbraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }



    
}

    void succodifrutta() {
hand_control_client_Ptr HandClient;
createHandClient(HandClient);

control_msgs::FollowJointTrajectoryGoal hand_goal;
arm_control_client_Ptr ArmClient;
createArmClient(ArmClient);
control_msgs::FollowJointTrajectoryGoal arm_goal;


double valorisuccobraccio[7] = {0.44,-0.44,-2.22,2.21,-1.95,0.22,0.12};
waypoints_arm_goal(arm_goal,valorisuccobraccio);
arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
ArmClient->sendGoal(arm_goal);
while(!(ArmClient->getState().isDone()) && ros::ok())
{
  ros::Duration(0.1).sleep();
}

double valorisuccomano[3] = {3.94,0.54,-2.26};
waypoints_hand_goal(hand_goal,valorisuccomano);

hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
HandClient->sendGoal(hand_goal);
while(!(HandClient->getState().isDone()) && ros::ok())
{
  ros::Duration(0.1).sleep();
}
        double valorisuccobraccio1[7] = {0.28,-0.55,-1.97,2.21,-1.95,0.22,-1.78};
        waypoints_arm_goal(arm_goal,valorisuccobraccio1);
        arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
        ArmClient->sendGoal(arm_goal);
        while(!(ArmClient->getState().isDone()) && ros::ok())
        {
          ros::Duration(0.1).sleep();
        }
        waypoints_arm_goal(arm_goal,valorisuccobraccio);
        arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
        ArmClient->sendGoal(arm_goal);
        while(!(ArmClient->getState().isDone()) && ros::ok())
        {
          ros::Duration(0.1).sleep();
        }
        waypoints_arm_goal(arm_goal,valorisuccobraccio1);
        arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
        ArmClient->sendGoal(arm_goal);
        while(!(ArmClient->getState().isDone()) && ros::ok())
        {
          ros::Duration(0.1).sleep();
        }
        
    }


void the() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorithebraccio[7] = {0.17,-0.59,-1.48,2.23,1.37,0.22,1.24};
    waypoints_arm_goal(arm_goal,valorithebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorithemano[3] = {3.94,0.54,-2.26};
    waypoints_hand_goal(hand_goal,valorithemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valorithebraccio1[7] = {0.17,-0.59,-1.13,2.23,1.37,0.22,1.24};
    waypoints_arm_goal(arm_goal,valorithebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }    
    waypoints_arm_goal(arm_goal,valorithebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    waypoints_arm_goal(arm_goal,valorithebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorithebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    

}
    
void gommadamasticare() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorigommadamasticarebraccio[7] = {0.07,0.16,-2.17,2.25,-1.04,0.25,0.12};
    waypoints_arm_goal(arm_goal,valorigommadamasticarebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorigommadamasticaremano[3] = {3.94,0.54,-2.26};
    waypoints_hand_goal(hand_goal,valorigommadamasticaremano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valorigommadamasticarebraccio1[7] = {0.07,-0.42,-2.17,2.28,-1.04,0.25,0.12};
    waypoints_arm_goal(arm_goal,valorigommadamasticarebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valorigommadamasticarebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorigommadamasticarebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valorigommadamasticarebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}

void birraallaspina() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoribirraallaspinabraccio[7] = {0.07,-0.5,-3.22,2.28,0,0.03,-1.37};
    waypoints_arm_goal(arm_goal,valoribirraallaspinabraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valoribirraallaspinamano[3] = {6,9,6};
    waypoints_hand_goal(hand_goal,valoribirraallaspinamano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && !(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valoribirraallaspinabraccio2[7] = {0.07,-0.5,-1.83,2.28,-0.75,0.03,-2.07};
    waypoints_arm_goal(arm_goal,valoribirraallaspinabraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

}

void patate() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;
    double valoripatatebraccio10[7] = {0.07,-1.02,-2.28,2.03,-1.53,0.28,0.12};
    waypoints_arm_goal2(arm_goal,valoripatatebraccio10);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(1.8);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valoripatatemano[3] = {3.94,4.54,-2.62};
    waypoints_hand_goal(hand_goal,valoripatatemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    HandClient->sendGoal(hand_goal);
    
    double valoripatatebraccio1[7] = {0.07,-0.57,-3.22,2.18,-1.53,0.28,0.12};
    waypoints_arm_goal(arm_goal,valoripatatebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoripatatebraccio2[7] = {0.07,-0.57,-3.22,1.77,-1.53,0.28,0.12};
    waypoints_arm_goal(arm_goal,valoripatatebraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    waypoints_arm_goal(arm_goal,valoripatatebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoripatatebraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}

void cibo() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    double valoricibobraccio[7] = {0.23,-1.12,-1.58,1.90,-1.04,0.25,-0.50};
    double valoricibomano[3] = {3.94,4.54,-2.62};
    double valoricibobraccio1[7] = {0.23,-1.12,-1.58,2.28,-1.04,0.25,-0.50};

    waypoints_arm_goal(arm_goal,valoricibobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    
    waypoints_hand_goal(hand_goal,valoricibomano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.9);
    HandClient->sendGoal(hand_goal);
    waypoints_arm_goal(arm_goal,valoricibobraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(1.4);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valoricibobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(2.1);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valoricibobraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(2.8);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
}

void bere() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoriberebraccio[7] = {0.23,-1.12,-1.58,1.90,-1.04,0.25,-1.58};
    double valoriberemano[3] = {6,9,2};
    double valoriberebraccio1[7] = {0.23,-1.12,-1.58,2.28,-1.04,0.25,-1.58};

    waypoints_arm_goal(arm_goal,valoriberebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    
    waypoints_hand_goal(hand_goal,valoriberemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.9);
    HandClient->sendGoal(hand_goal);
    waypoints_arm_goal(arm_goal,valoriberebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(1.4);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valoriberebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(2.1);
    ArmClient->sendGoal(arm_goal);
    waypoints_arm_goal(arm_goal,valoriberebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(2.8);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    
}


void panino() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoripaninobraccio[7] = {0.49,-0.17,-2.28,2.29,-1.78,0.28,-1.49};
    waypoints_arm_goal(arm_goal,valoripaninobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    
    double valoripaninomano[3] = {2.57,2.89,-0.93};
    waypoints_hand_goal(hand_goal,valoripaninomano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoripaninobraccio2[7] = {0.49,-0.17,-2.28,1.95,-1.78,0.28,-0.33};
    waypoints_arm_goal(arm_goal,valoripaninobraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoripaninobraccio3[7] = {0.49,-0.17,-2.28,2.29,-1.78,0.28,-1.49};
    waypoints_arm_goal(arm_goal,valoripaninobraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoripaninobraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}




void tovagliolo() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoritovagliolobraccio[7] = {0.23,-0.77,-2.07,2.28,-1.91,0.17,0.54};
    waypoints_arm_goal(arm_goal,valoritovagliolobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valoritovagliolomano[3] = {0,0,0};
    waypoints_hand_goal(hand_goal,valoritovagliolomano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valoritovagliolobraccio1[7] = {0.30,-0.47,-2.17,2.28,-1.91,0.17,0.54};
    waypoints_arm_goal(arm_goal,valoritovagliolobraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);
    
    double valoritovagliolobraccio2[7] = {0.23,-0.77,-2.07,2.28,-1.91,0.17,0.54};
    waypoints_arm_goal(arm_goal,valoritovagliolobraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.5);
    ArmClient->sendGoal(arm_goal);

    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoritovagliolobraccio3[7] = {0.30,-0.47,-2.17,2.28,-1.91,0.17,0.54};
    waypoints_arm_goal(arm_goal,valoritovagliolobraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}

void euro() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorieurobraccio[7] = {0.62,-1.25,-2.42,2.28,2.04,-0.5,-0.5};
    waypoints_arm_goal(arm_goal,valorieurobraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorieuromano[3] = {4.14,6.19,-2.62};
    waypoints_hand_goal(hand_goal,valorieuromano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valorieurobraccio1[7] = {0.62,-1.50,-2.17,2.28,2.04,-0.64,-0.58};
    waypoints_arm_goal(arm_goal,valorieurobraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorieurobraccio2[7] = {0.57,-1.35,-2.62,2.28,2.04,-0.64,-0.12};
    waypoints_arm_goal(arm_goal,valorieurobraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valorieurobraccio3[7] = {0.62,-1.15,-2.42,2.28,2.04,-0.50,-0.33};
    waypoints_arm_goal(arm_goal,valorieurobraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
}

void cocacola() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoricocabraccio[7] = {0.33,-0.95,-2.37,2.28,-2.07,0.31,-1.45};
    waypoints_arm_goal(arm_goal,valoricocabraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valoricocamano[3] = {2.27,9,-1.46};
    waypoints_hand_goal(hand_goal,valoricocamano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valoricocabraccio1[7] = {0.49,-1.10,-2.62,2.13,-1.70,0.31,-1.45};
    waypoints_arm_goal(arm_goal,valoricocabraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}
void wc() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoriwcbraccio[7] = {0.80,-0.97,-2.92,2.28,-2.07,0,-1.94};
    waypoints_arm_goal(arm_goal,valoriwcbraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valoriwcmano[3] = {3.06,0.89,-2.62};
    waypoints_hand_goal(hand_goal,valoriwcmano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

}
void miscusiconto() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoriscusibraccio[7] = {0.46, -0.82, -2.27, 2.27, 0.00 , -0.05, -1.53};
    double valoriscusibraccio1[7] = {0.46, -0.54, -2.27, 2.27, 0.00 , -0.05, -1.53};
    double valoriscusibraccio2[7] = { 0.46, -0.95, -1.92, 2.27, -1.62 , -0.05, -2.07};
    double valoriscusibraccio3[7] = { 0.12, -1.00, -1.58, 2.27, -1.91 , -0.05, -2.07};
    double valoriscusimano1[3] = {3.94,0.54,-2.26};
    waypoints_arm_goal(arm_goal,valoriscusibraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valoriscusimano[3] = {6,9,6};
    waypoints_hand_goal(hand_goal,valoriscusimano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriscusibraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriscusibraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriscusibraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_hand_goal(hand_goal,valoriscusimano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriscusibraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valoriscusibraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    

}

void dopo() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorifrullatorbraccio[7] = {0.62,-0.65,-2.12,2.05,-2.07,0.25,0.12};
    waypoints_arm_goal(arm_goal,valorifrullatorbraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorifrullatormano[3] = {0,9,6};
    waypoints_hand_goal(hand_goal,valorifrullatormano);
    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valorifrullatorbraccio1[7] = {0.62,-0.65,-2.12,2.05,-2.07,-0.20,1.24};
    waypoints_arm_goal(arm_goal,valorifrullatorbraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);

    double valorifrullatorbraccio2[7] = {0.62,-0.65,-2.12,2.05,-1,0.11,2.07};
    waypoints_arm_goal(arm_goal,valorifrullatorbraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.5);
    ArmClient->sendGoal(arm_goal);

    double valorifrullatorbraccio3[7] = {0.62,-0.65,-2.12,2.05,2,0.11,2.07};
    waypoints_arm_goal(arm_goal,valorifrullatorbraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
}

void frullatore() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorifrullatorebraccio[7] = {0.62,-0.65,-2.12,2.05,-2.07,0.25,0.12};
    waypoints_arm_goal(arm_goal,valorifrullatorebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorifrullatoremano[3] = {0,9,6};
    waypoints_hand_goal(hand_goal,valorifrullatoremano);
    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    double valorifrullatorebraccio1[7] = {0.62,-0.65,-2.12,2.05,-2.07,-0.20,1.24};
    waypoints_arm_goal(arm_goal,valorifrullatorebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.3);
    ArmClient->sendGoal(arm_goal);

    double valorifrullatorebraccio2[7] = {0.62,-0.65,-2.12,2.05,-1,0.11,2.07};
    waypoints_arm_goal(arm_goal,valorifrullatorebraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.5);
    ArmClient->sendGoal(arm_goal);

    double valorifrullatorebraccio3[7] = {0.62,-0.65,-2.12,2.05,2,0.11,2.07};
    waypoints_arm_goal(arm_goal,valorifrullatorebraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    waypoints_arm_goal(arm_goal,valorifrullatorebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.4);
    ArmClient->sendGoal(arm_goal);

    waypoints_arm_goal(arm_goal,valorifrullatorebraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

}
void lattefreddo() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorilattefbraccio[7] = {0.07, -0.67, -2.02, 2.21, -2.07 , -0.08,0.83};
    waypoints_arm_goal(arm_goal,valorilattefbraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valorilattefmano[3] = {-0.10, 0.18, -1.02};
    waypoints_hand_goal(hand_goal,valorilattefmano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }

    double valorilattefbraccio1[7] = {0.07, -0.67, -1.72, 2.21, -2.07 , -0.08,0.83};
    waypoints_arm_goal(arm_goal,valorilattefbraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valorilattefmano1[3] = {6,9,6};
    waypoints_hand_goal(hand_goal,valorilattefmano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorilattefbraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    waypoints_hand_goal(hand_goal,valorilattefmano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorilattefbraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    waypoints_hand_goal(hand_goal,valorilattefmano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    double valorilattefbraccio2[7] = {0.07,-1,-1.63,2.28,-2.07,-0.22,-0.33};
    double valorilattefbraccio3[7] = {0.28,-1,-1.63,2.28,-2.07,-0.22,-0.33};
    double valorilattefmano3[3] = {6,9,6};
    waypoints_arm_goal(arm_goal,valorilattefbraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    waypoints_hand_goal(hand_goal,valorilattefmano3);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }

    waypoints_arm_goal(arm_goal,valorilattefbraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorilattefbraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorilattefbraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    
}
void lattecaldo() { hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valorilattebraccio[7] = {0.07, -0.67, -2.02, 2.21, -2.07 , -0.08,0.83};
    waypoints_arm_goal(arm_goal,valorilattebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valorilattemano[3] = {-0.10, 0.18, -1.02};
    waypoints_hand_goal(hand_goal,valorilattemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }

    double valorilattebraccio1[7] = {0.07, -0.67, -1.72, 2.21, -2.07 , -0.08,0.83};
    waypoints_arm_goal(arm_goal,valorilattebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valorilattemano1[3] = {6,9,6};
    waypoints_hand_goal(hand_goal,valorilattemano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorilattebraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    waypoints_hand_goal(hand_goal,valorilattemano);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorilattebraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    waypoints_hand_goal(hand_goal,valorilattemano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    double valorilattebraccio2[7] = {0.07, -0.67, -1.72, 2.21, -2.07 , -0.08,0.83};
    double valorilattebraccio3[7] = {0.07, -0.67, -1.72, 2.28, -2.07 , 0.22,0.83};
    double valorilattemano3[3] = {3.1,3.3,0};
    waypoints_arm_goal(arm_goal,valorilattebraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    waypoints_hand_goal(hand_goal,valorilattemano3);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal); 

    waypoints_arm_goal(arm_goal,valorilattebraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    waypoints_arm_goal(arm_goal,valorilattebraccio2);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);

    waypoints_arm_goal(arm_goal,valorilattebraccio3);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
    
}
void limone() {
        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

    
    double valoribraccio[7] = {0.23,-0.82,-1.73,2.28,-2.07,0.31,0.04};
    waypoints_arm_goal(arm_goal,valoribraccio);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valoribraccio1[7] = {0.23,-0.82,-1.53,2.28,-2.07,0.31,0.04};
    waypoints_arm_goal(arm_goal,valoribraccio1);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    double valorimano1[3] = {6,9,6};
    waypoints_hand_goal(hand_goal,valorimano1);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok() && !(HandClient->getState().isDone()))
    {
      ros::Duration(0.1).sleep();
    }
}

void buongiorno() {

        hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

     double valoribraccio3[7] = {0.54, -0.62, -2.52,2.29, -1.16 , -0.25, -0.17};
     waypoints_arm_goal(arm_goal,valoribraccio3);
     arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     ArmClient->sendGoal(arm_goal);
     double valorimano5[3] = {5,5,5};
     waypoints_hand_goal(hand_goal,valorimano5);

     hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     HandClient->sendGoal(hand_goal);
     while(!(ArmClient->getState().isDone()) && ros::ok())
     {
       ros::Duration(0.1).sleep();
     }
     double valoribraccio2[7] = {0.54, -0.62, -2.52, 1.90, -1.16 , -0.25, -0.17};
     waypoints_arm_goal(arm_goal,valoribraccio2);
     arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     ArmClient->sendGoal(arm_goal);
     
     double valorimano3[3] = {0,0,0};
     waypoints_hand_goal(hand_goal,valorimano3);

     hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     HandClient->sendGoal(hand_goal);
     
     while(!(HandClient->getState().isDone()) && ros::ok() && !(ArmClient->getState().isDone()))
     {
       ros::Duration(0.1).sleep();
     }
     double valoribraccio4[7] = {0.54, -0.62, -3.02, 1.90, -1.16 , -0.25, 0.50};
     waypoints_arm_goal(arm_goal,valoribraccio4);
     arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     ArmClient->sendGoal(arm_goal);
     
     while(!(HandClient->getState().isDone()) && ros::ok() && !(ArmClient->getState().isDone()))
     {
       ros::Duration(0.1).sleep();
     }
     
}

void desidera() {
            hand_control_client_Ptr HandClient;
    createHandClient(HandClient);

    control_msgs::FollowJointTrajectoryGoal hand_goal;
    arm_control_client_Ptr ArmClient;
    createArmClient(ArmClient);
    control_msgs::FollowJointTrajectoryGoal arm_goal;

     double valoribraccio3[7] = {0.54, -0.62, -2.52,2.29, -1.16 , -0.25, -0.17};
     waypoints_arm_goal(arm_goal,valoribraccio3);
     arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     ArmClient->sendGoal(arm_goal);
     while(!(ArmClient->getState().isDone()) && ros::ok())
     {
       ros::Duration(0.1).sleep();
     }
     
     double valorimano3[3] = {5, 5, 5};
     waypoints_hand_goal(hand_goal,valorimano3);

     hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     HandClient->sendGoal(hand_goal);
     while(!(HandClient->getState().isDone()) && ros::ok())
     {
       ros::Duration(0.1).sleep();
     }
     double valoribraccio4[7] = {0.54, -0.62, -2.52,2.29, -1.16 , -0.25, -0.17};
     waypoints_arm_goal(arm_goal,valoribraccio4);
     arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     ArmClient->sendGoal(arm_goal);
     while(!(ArmClient->getState().isDone()) && ros::ok())
     {
       ros::Duration(0.1).sleep();
     }
     
     double valorimano4[3] = {0,-0.06,-1.10};
     waypoints_hand_goal(hand_goal,valorimano4);

     hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     HandClient->sendGoal(hand_goal);
     while(!(HandClient->getState().isDone()) && ros::ok())
     {
       ros::Duration(0.1).sleep();
     }
    
     double valoribraccio7[7] = {0.59, -1.00, -2.87, 2.05, -1.16 , 0.20, 0.30};
     waypoints_arm_goal(arm_goal,valoribraccio7);
     arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     ArmClient->sendGoal(arm_goal);
     while(!(ArmClient->getState().isDone()) && ros::ok())
     {
       ros::Duration(0.1).sleep();
     }
    double valoribracciointer[7] = {0.59, -1.00, -2.87, 2.05, -1.16 , 0.20, 0.71};
    waypoints_arm_goal(arm_goal,valoribracciointer);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
     
     double valorimano7[3] = {0,-0.06,-1.10};
     waypoints_hand_goal(hand_goal,valorimano7);

     hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
     HandClient->sendGoal(hand_goal);
     while(!(HandClient->getState().isDone()) && ros::ok())
     {
       ros::Duration(0.1).sleep();
     }

    double valoribraccio13[7] = {0.77, -0.87, -2.57, 1.80, -1.16 , -0.42, -0.08};
    waypoints_arm_goal(arm_goal,valoribraccio13);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorimano13[3] = {0,-1.10,-1.10};
    waypoints_hand_goal(hand_goal,valorimano13);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    double valoribraccio14[7] = {0.35, -0.87, -2.57, 1.80, -1.16 , -0.42, -0.08};
    waypoints_arm_goal(arm_goal,valoribraccio14);
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    ArmClient->sendGoal(arm_goal);
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }
    
    double valorimano14[3] = {0,-1.10,-1.10};
    waypoints_hand_goal(hand_goal,valorimano14);

    hand_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.7);
    HandClient->sendGoal(hand_goal);
    while(!(HandClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.1).sleep();
    }

    
}