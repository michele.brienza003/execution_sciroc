# SciRoc Competition 2021 - Unibas Team
In this repository there is the UniBas Team's source code about SciRoc Challenge 2021 for the Episode 2 "Italian Sign Language Execution (LIS) ", using the TIAGo robot.

# Prerequisites

- Ubuntu 18.04


## Clone the repository

First step consists in cloning the source code with the command: 

git clone https://gitlab.com/michele.brienza003/execution_sciroc

## Build image

After cloning , in directory of repository execute following command:

cd competition
docker build -t competition .

## Configure pal_docker_utils

Clone also repository of pal robotics

git clone https://github.com/pal-robotics/pal_docker_utils/

and after

- Copy name of image after build.

After configuring pal_docker_utils, you will need to execute the pal_docker.sh script with the name of the image.

In a terminal 

cd pal_docker_utils/scripts
sudo ./pal_docker.sh -it YOUR_DOCKER_IMAGE


- You can read the name of image with command docker image list and copy id of image with name competition

## Start the simulation

roslaunch demo_motions sciroc_ep2_execution.launch

This launch file start simulation in gazebo, open the listener node of message of GUI and the node for execution of sign.



