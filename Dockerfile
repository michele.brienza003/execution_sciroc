FROM registry.gitlab.com/competitions4/sciroc/dockers/sciroc:1.5

ARG REPO_WS=/ws
RUN mkdir -p $REPO_WS/src
WORKDIR /home/user/$REPO_WS

RUN apt-get update

RUN apt install nano -y

RUN apt-get install -y python3.8

RUN apt install python3-pip -y

RUN apt install python-pip -y

RUN pip3 install appJar

RUN pip install rospkg

RUN apt-get install python3-tk -y

COPY ./ws /home/user/ws/src

RUN bash -c "source /opt/pal/ferrum/setup.bash \
    && catkin build \
    && echo 'source /opt/pal/ferrum/setup.bash' >> ~/.bashrc \
    && echo 'source devel/setup.bash' >> ~/.bashrc "


ENTRYPOINT ["bash"]
